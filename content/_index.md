+++
title = "Julia Scheaffer"
+++
## About me
I am recent graduate from Dakota State University. I enjoy programming computers
and using Linux. I also like wrting programming languages and reading about
programming language theory.

Most my code is available on my [Gitlab][2].

## Blog
You can read my blog posts [here](blog)

## Contact
You can contact me at [julia.scheaffer@gmail.com](mailto:julia.scheaffer@gmail.com)

[1]: https://pcpartpicker.com/user/pythondude325/saved/R3ZQZL
[2]: https://gitlab.com/pythongirl
